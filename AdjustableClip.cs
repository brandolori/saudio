using UnityEngine;

[System.Serializable]
public class AdjustableClip
{
    [SerializeField]
    AudioClip clip = default;

    [SerializeField]
    [Range(0f, 1f)]
    float volume = .5f;

    [SerializeField]
    [Range(0f, 1f)]
    float volumeVariance = .1f;

    [SerializeField]
    [Range(.5f, 1.5f)]
    float pitch = 1f;

    [SerializeField]
    [Range(0f, 1f)]
    float pitchVariance = .1f;

    public AudioClip Clip { get => clip; }
    public float Volume { get => volume; }
    public float VolumeVariance { get => volumeVariance; }
    public float Pitch { get => pitch; }
    public float PitchVariance { get => pitchVariance; }
}


