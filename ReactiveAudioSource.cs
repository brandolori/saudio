﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Bran.SAudio
{
    public class ReactiveAudioSource : MonoBehaviour
    {
        AudioSource source;
        bool playing;
        Action action;

        void Awake()
        {
            this.source = gameObject.AddComponent<AudioSource>();
        }

        void Update()
        {
            if (this.playing)
            {
                if (!this.source.isPlaying)
                {
                    this.playing = false;
                    this.action.Invoke();
                }
            }
        }

        public void Play(AdjustableClip clip, float masterVolume = 1.0f, bool loop = false, Action whenStopped = default)
        {
            this.source.clip = clip.Clip;
            this.source.volume = (clip.Volume + UnityEngine.Random.Range(-clip.VolumeVariance, clip.VolumeVariance)) * masterVolume;
            this.source.pitch = clip.Pitch + UnityEngine.Random.Range(-clip.PitchVariance, clip.PitchVariance);
            this.source.loop = loop;
            this.source.Play();
            this.action = whenStopped ?? (() => { });
            this.playing = true;
        }

        public void Stop()
        {
            this.source.Stop();
        }

    }
}
