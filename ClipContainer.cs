using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bran.SAudio
{
    enum SelectionPolicy
    {
        RANDOM,
        SEQUENTIAL
    }

    public enum ClipType
    {
        SEGMENTED,
        HOMOGENEOUS
    }

    public struct IntroOutroClip
    {
        AdjustableClip intro;
        AdjustableClip middle;
        AdjustableClip outro;

        public IntroOutroClip(AdjustableClip intro, AdjustableClip middle, AdjustableClip outro)
        {
            this.intro = intro;
            this.middle = middle;
            this.outro = outro;
        }

        public AdjustableClip Intro { get => intro; }
        public AdjustableClip Middle { get => middle; }
        public AdjustableClip Outro { get => outro; }
    }

    [CreateAssetMenu]
    public class ClipContainer : ScriptableObject
    {
        [SerializeField]
        SelectionPolicy selectionPolicy = default;

        [SerializeField]
        ClipType clipType = default;

        [SerializeField]
        AdjustableClip introClip = default;

        [SerializeField]
        AdjustableClip[] clips = default;

        [SerializeField]
        AdjustableClip outroClip = default;

        [SerializeField]
        [Range(.5f, 1.5f)]
        float masterVolume = 1f;

        [SerializeField]
        bool loop = false;

        int clipIndex = default;

        public AdjustableClip Current { get => clips[this.clipIndex % clips.Length]; }
        public IntroOutroClip Clips { get => new IntroOutroClip(this.introClip, this.Current, this.outroClip); }
        public ClipType Type { get => this.clipType; }
        public bool Loop { get => loop; }
        public float MasterVolume { get => masterVolume; }
        public bool Populated { get => this.clips.Length > 0; }
        
        public void AdvanceClip()
        {
            if (this.selectionPolicy == SelectionPolicy.SEQUENTIAL)
            {
                this.clipIndex = (this.clipIndex + 1) % this.clips.Length;
            }
            else
            {
                this.clipIndex = Random.Range(0, this.clips.Length);
            }
        }
    }
}

