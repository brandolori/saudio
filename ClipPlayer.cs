using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace Bran.SAudio
{
    public class ClipPlayer : MonoBehaviour
    {
        ReactiveAudioSource audioSource;

        [SerializeField]
        ClipContainer container;

        [SerializeField]
        bool playOnStart = default;

        void Awake()
        {
            this.audioSource = gameObject.AddComponent<ReactiveAudioSource>();
        }

        void Start()
        {
            if (playOnStart)
            {
                this.Play();
            }
        }

        public void Init(ClipContainer clip)
        {
            this.container = clip;
        }

        public void Play()
        {
            if (!container)
            {
                throw new InvalidOperationException("player not initialized");
            }
            if (this.container.Populated)
            {
                if (this.container.Type == ClipType.SEGMENTED)
                {
                    this.audioSource.Play(this.container.Clips.Intro, masterVolume: this.container.MasterVolume, whenStopped: () =>
                    {
                        this.audioSource.Play(this.container.Clips.Middle, masterVolume: this.container.MasterVolume, loop: this.container.Loop, whenStopped: () =>
                         {
                             this.audioSource.Play(this.container.Clips.Outro, masterVolume: this.container.MasterVolume);
                         });
                    });
                }
                else
                {
                    this.audioSource.Play(this.container.Current, masterVolume: this.container.MasterVolume, loop: this.container.Loop);
                }
                this.container.AdvanceClip();
            }
            else
            {
                print($"ClipContainer not populated - {this.container}");
            }
        }

        public void Stop()
        {
            if (!container)
            {
                throw new InvalidOperationException("player not initialized");
            }
            audioSource.Stop();
        }
    }
}