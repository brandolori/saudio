﻿using UnityEngine;
using UnityEditor;
using Malee.Editor;

namespace Bran.SAudio
{

    [CustomEditor(typeof(ClipContainer))]
    [CanEditMultipleObjects]
    public class ClipContainerEditor : Editor
    {
        SerializedProperty clipType, introClip, outroClip, clips, masterVolume, selectionPolicy, loop;
        ReorderableList clipsReorderable;

        void OnEnable()
        {
            clipType = serializedObject.FindProperty("clipType");
            introClip = serializedObject.FindProperty("introClip");
            outroClip = serializedObject.FindProperty("outroClip");
            masterVolume = serializedObject.FindProperty("masterVolume");
            selectionPolicy = serializedObject.FindProperty("selectionPolicy");
            loop = serializedObject.FindProperty("loop");
            clips = serializedObject.FindProperty("clips");
            clipsReorderable = new ReorderableList(clips);
            clipsReorderable.elementNameProperty = "clip";
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(clipType);

            if (clipType.enumValueIndex == 0)
            {
                EditorGUILayout.PropertyField(introClip, true);
                EditorGUILayout.PropertyField(outroClip);
            }

            EditorGUILayout.PropertyField(selectionPolicy);

            clipsReorderable.DoLayoutList();

            EditorGUILayout.PropertyField(loop);
            EditorGUILayout.PropertyField(masterVolume);


            serializedObject.ApplyModifiedProperties();
        }
    }
}